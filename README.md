# preprocess4gaze
A preprocessing pipeline for producing datasets for gaze direction estimation.

## Setup

### 1. Clone the repository
Either use

    git clone git@bitbucket.org:swook/preprocess4gaze.git --recursive

or after a normal clone, initialize the submodules

    git clone git@bitbucket.org:swook/preprocess4gaze.git
    git submodule update --init --recursive

### 2. Pull additional files
The pipeline depends on a few additional files which are not included in the repository nor submodules. Grab them by running

    bash prepare.bash

### 3. Edit the main script

Open `src/main.py` and update the dataset paths. Check out the derived classes in `src/datasets` to confirm the expected folder tree structure for a given dataset. For instance for the dataset [**MPIIFaceGaze**](https://www.mpi-inf.mpg.de/departments/computer-vision-and-multimodal-computing/research/gaze-based-human-computer-interaction/its-written-all-over-your-face-full-face-appearance-based-gaze-estimation/), the [raw dataset](http://datasets.d2.mpi-inf.mpg.de/MPIIGaze/MPIIFaceGaze.zip) and not the preprocessed version should be acquired. The `dataset_path` have at its top-level the subject specific paths `p00/` to `p14/`.

## Run
Once a valid path has been input, you should be able to run the pipeline.

    cd src/
    python3 main.py
