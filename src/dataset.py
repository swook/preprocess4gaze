"""
Copyright 2019 ETH Zurich, Seonwook Park

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import traceback

import cv2 as cv
import eos
import h5py
import matplotlib.pyplot as plt
import numpy as np
from sklearn.externals import joblib
import tensorflow as tf
import transforms3d as t3d

from face import FaceDetector
from landmark import LandmarksDetector
from head import EosHeadPoseEstimator, PnPHeadPoseEstimator
import normalization as norm

import logging
logger = logging.getLogger(__name__)


class Dataset(object):

    def __init__(self, dataset_folder):
        assert os.path.isdir(dataset_folder)
        self.dataset_name = os.path.basename(os.path.normpath(dataset_folder))
        self.step_1_input_dir = dataset_folder
        self.step_1_output_dir = self.step_2_input_dir = \
                '../outputs/%s/intermediate_1_faces' % self.dataset_name
        self.step_2_output_dir = self.step_3_input_dir = \
                '../outputs/%s/intermediate_2_landmarks' % self.dataset_name
        self.step_3_output_dir = self.step_4_input_dir = \
                '../outputs/%s/intermediate_3_head_pose' % self.dataset_name
        self.step_4_output_dir = \
                '../outputs/%s' % self.dataset_name

        # Get a list of directories specific to persons.
        # For an n-participant dataset, there should be n folders.
        self.person_dirs = sorted(self.get_person_dirs())
        for person_dir in self.person_dirs:
            assert os.path.isdir(dataset_folder + '/' + person_dir)
        logger.info('%d people found in %s.' % (len(self.person_dirs), self.dataset_name))

        self.maybe_do_step_1_detect_faces()
        self.maybe_do_step_2_detect_landmarks()
        self.maybe_do_step_3_head_pose_estimation()
        self.maybe_do_step_4_final_outputs()

    def maybe_do_step_1_detect_faces(self):
        try:
            self.check_if_expected_outputs_exist(self.step_1_output_dir, '.pkl')
        except:
            self.step_1_detect_faces(self.step_1_input_dir, self.step_1_output_dir)
            tf.reset_default_graph()

    def check_if_expected_outputs_exist(self, output_dir, expected_suffix):
        """All expected files need to exist, even if a single one is missing, re-do."""
        for person_id in self.person_dirs:
            output_file = output_dir + '/' + person_id + expected_suffix
            if not os.path.isfile(output_file):
                raise Exception('Expected output "%s" not found.' % output_file)

    def get_person_dirs(self):
        raise NotImplementedError('Dataset::get_person_dirs not implemented.')

    def step_1_detect_faces(self, input_dir, output_dir):
        """First step of preprocessing pipeline: detect faces."""
        assert_and_ensure_dirs_exist(input_dir, output_dir)
        face_detect_batch_size = 16
        face_detector = FaceDetector(batch_size=face_detect_batch_size)

        # Detect largest face bounding box per frame in person_dir
        for person_id in self.person_dirs:
            # Skip if already processed person
            person_pkl = '%s/%s.pkl' % (output_dir, person_id)
            if os.path.isfile(person_pkl) and os.stat(person_pkl).st_size > 10:
                logger.info('%s seems to have been processed already.' % person_id)
                continue

            # The preprocessing function is expected to yield the
            # (a) undistorted full image frame in RGB (not BGR),
            # (b) gaze target position (in 3D wrt camera coordinates system),
            # (c) and camera intrinsic parameters (fx, fy, cx, cy)
            person_data = self.preprocess_person_dir(input_dir + '/' + person_id)
            person_data = person_data if person_data is not None else []
            for entry in person_data:
                assert 'full_frame' in entry
                assert '3d_gaze_target' in entry
                assert 'camera_parameters' in entry

            # Detect face, save crop only, and remove full frame
            num_entries = len(person_data)
            num_batches = int(np.ceil(num_entries / face_detect_batch_size))
            successful_entries = []
            if num_batches == 1:
                logger.error('There are not enough entries for %s' % person_id)
                num_batches = 0
            for i in range(num_batches):
                a = i * face_detect_batch_size
                z = a + face_detect_batch_size
                is_last_batch = (i == num_batches - 1)
                if is_last_batch:
                    num_last_entries = num_entries - a
                    z = num_entries
                    a = z - face_detect_batch_size
                batch_entries = person_data[a:z]
                bboxes = face_detector.detect_batch([
                    entry['full_frame'] for entry in batch_entries
                ])
                if is_last_batch:
                    bboxes = bboxes[-num_last_entries:]
                    batch_entries = batch_entries[-num_last_entries:]
                for bbox, entry in zip(bboxes, batch_entries):
                    if bbox is None:
                        logger.error('Skipping one image due to no face found.')
                        continue

                    frame = entry['full_frame']
                    h, w, _ = frame.shape
                    u0, v0, un, vn, _ = bbox
                    uc, vc = int(np.round(0.5 * (u0 + un))), int(np.round(0.5 * (v0 + vn)))
                    w_2 = int(np.ceil(1.3 * 0.5 * max(un - u0, vn - v0)))
                    u0, v0, un, vn = uc - w_2, vc - w_2, uc + w_2, vc + w_2
                    if u0 < 0 or un > w:  # Correct if beyond x-axis boundaries
                        diff = -u0 if u0 < 0 else w - un
                        u0 += diff
                        un += diff
                    if v0 < 0 or vn > h:  # Correct if beyond y-axis boundaries
                        diff = -v0 if v0 < 0 else h - vn
                        v0 += diff
                        vn += diff
                    entry['full_face'] = frame[v0:vn, u0:un, :]
                    entry['face_bounding_box'] = (u0, v0, un-u0, vn-v0)
                    entry['full_frame_size'] = (w, h)
                    successful_entries.append(entry)
                # # Visualise last face
                # cv.imshow('face', cv.resize(cv.cvtColor(entry['full_face'],
                #                             cv.COLOR_RGB2BGR), (448, 448)))
                # cv.waitKey(1)
            person_data = successful_entries

            # Remove full frames now, no longer necessary
            for entry in person_data:
                del entry['full_frame']

            # Store to per-person joblib file
            if len(person_data) == 0:
                logger.info('No data found for %s::%s' % (self.dataset_name, person_id))
            for entry in person_data:
                assert 'full_frame' not in entry
                assert 'full_face' in entry
                assert 'face_bounding_box' in entry
            with open(person_pkl, 'wb') as f:
                joblib.dump(person_data, f)
                logger.info('Stored %s' % person_pkl)

    def preprocess_person_dir(self, person_dir):
        raise NotImplementedError('Dataset::preprocess_person_dir not implemented.')

    def maybe_do_step_2_detect_landmarks(self):
        try:
            self.check_if_expected_outputs_exist(self.step_2_output_dir, '.pkl')
        except:
            self.step_2_detect_landmarks(self.step_2_input_dir, self.step_2_output_dir)
            tf.reset_default_graph()

    def step_2_detect_landmarks(self, input_dir, output_dir):
        assert_and_ensure_dirs_exist(input_dir, output_dir)
        landmarks_detect_batch_size = 16
        landmarks_detector = LandmarksDetector(batch_size=landmarks_detect_batch_size)

        for person_id in self.person_dirs:
            input_pkl = '%s/%s.pkl' % (input_dir, person_id)
            with open(input_pkl, 'rb') as f:
                input_data = joblib.load(f)

            # Detect facial landmarks
            num_entries = len(input_data)
            num_batches = int(np.ceil(num_entries / landmarks_detect_batch_size))
            if num_batches == 1:
                logger.error('There are not enough entries for %s' % person_id)
                num_batches = 0
            output_data = []
            for i in range(num_batches):
                a = i * landmarks_detect_batch_size
                z = a + landmarks_detect_batch_size
                is_last_batch = (i == num_batches - 1)
                if is_last_batch:
                    num_last_entries = num_entries - a
                    z = num_entries
                    a = z - landmarks_detect_batch_size
                batch_entries = input_data[a:z]
                landmarks, maxs = landmarks_detector.detect_batch([
                    entry['full_face'] for entry in batch_entries
                ])
                if is_last_batch:
                    landmarks = landmarks[-num_last_entries:, :]
                    maxs = maxs[-num_last_entries:]
                    batch_entries = batch_entries[-num_last_entries:]
                for l, m in zip(landmarks, maxs):
                    output_data.append((l, m))

            # Store landmark coordinates
            output_pkl = '%s/%s.pkl' % (output_dir, person_id)
            with open(output_pkl, 'wb') as f:
                joblib.dump(output_data, f)
                logger.info('Stored landmarks in %s' % output_pkl)

    def maybe_do_step_3_head_pose_estimation(self):
        try:
            self.check_if_expected_outputs_exist(self.step_3_output_dir, '.pkl')
        except:
            self.step_3_head_pose_estimation(self.step_3_input_dir, self.step_3_output_dir)

    def step_3_head_pose_estimation(self, input_dir, output_dir):
        assert_and_ensure_dirs_exist(input_dir, output_dir)
        head_pose_estimator = PnPHeadPoseEstimator()

        for person_id in self.person_dirs:
            landmarks_pkl = '%s/%s.pkl' % (input_dir, person_id)
            with open(landmarks_pkl, 'rb') as f:
                landmarks = joblib.load(f)

            raw_data_pkl = '%s/%s.pkl' % (self.step_1_output_dir, person_id)
            with open(raw_data_pkl, 'rb') as f:
                person_data = joblib.load(f)

            # DO HEAD POSE ESTIMATION HERE
            # 0-based indices to know from 86-landmarks (from camera perspective)
            # L-eye L-corner: 36
            # L-eye R-corner: 39
            # R-eye L-corner: 42
            # R-eye R-corner: 45
            # Nose ridge (up-to-down): 27 - 30
            # Nose base (left-to-right): 31 - 35
            output_data = []
            for entry, (_landmarks, scores) in zip(person_data, landmarks):
                bu, bv, bw, bh = entry['face_bounding_box']
                _landmarks += np.asarray([[bu, bv]])

                scores_that_matter = np.array([
                    scores[i - 1] for i in head_pose_estimator.ibug_ids_to_use
                ])
                if np.any(scores_that_matter < 0.7):
                    rvec = tvec = None
                else:
                    rvec, tvec = head_pose_estimator.fit_func(
                        _landmarks, entry['camera_parameters'],
                    )
                    if tvec[2] > 2000:  # Beyond 2 meters
                        rvec = tvec = None
                output_data.append((rvec, tvec))

                # if len(output_data) % 50 == 0 and rvec is not None and tvec is not None:
                #     visualize_head_box(head_pose_estimator, entry, _landmarks, rvec, tvec)

            """
            # Visualize head pose distribution
            head_pose = np.array([
                list(t3d.euler.mat2euler(cv.Rodrigues(rvec)[0])) + list(tvec)
                for rvec, tvec in output_data
                if rvec is not None and tvec is not None
            ])
            plt.ion()
            if len(plt.get_fignums()) == 0:
                plt.figure(figsize=(15, 5))
            else:
                plt.clf()
            plt.subplot(131)
            plt.hist2d(head_pose[:, 0], head_pose[:, 1], bins=30)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.subplot(132)
            plt.hist2d(head_pose[:, 1], head_pose[:, 2], bins=30)
            plt.xlabel('y')
            plt.ylabel('z')
            plt.subplot(133)
            plt.hist(head_pose[:, 5], bins=30)
            plt.xlabel('d')
            plt.show(block=False)
            plt.pause(0.001)
            """

                # # Use eos for 3DMM fitting
                # eos_mesh, eos_pose, eos_shape_coeffs, eos_blendshape_coeffs = \
                #         head_pose_estimator.fit_func(_landmarks, (fw, fh))
                # output_data.append({
                #     'eos_pose': eos_pose,
                #     'eos_shape_coeffs': eos_shape_coeffs,
                #     'eos_blendshape_coeffs': eos_blendshape_coeffs,
                # })
                # full_frame = np.zeros((fh, fw, 3), dtype=np.uint8)
                # full_frame[bv:bv+bh, bu:bu+bw, :] = cv.cvtColor(entry['full_face'], cv.COLOR_RGB2BGR)
                # print('modelview: \n%s' % eos_pose.get_modelview())
                # print('projection: \n%s' % eos_pose.get_projection())
                # print(np.matmul(eos_pose.get_projection(), eos_pose.get_modelview()))
                # print(eos_pose.get_rotation_euler_angles())
                # print()
                # isomap = eos.render.extract_texture(eos_mesh, eos_pose, full_frame)

                # # Draw landmarks
                # face_image = cv.cvtColor(entry['full_face'], cv.COLOR_RGB2BGR)
                # vis_size = isomap.shape[0]
                # scale = float(vis_size) / face_image.shape[0]
                # face_image = cv.resize(face_image, (vis_size, vis_size))
                # _landmarks -= np.asarray([[bu, bv]])
                # for i, (u, v) in enumerate(_landmarks):
                #     u = int(scale * u)
                #     v = int(scale * v)
                #     cv.drawMarker(face_image, (u, v), (0, 255, 0),
                #                   cv.MARKER_DIAMOND, markerSize=5, line_type=cv.LINE_AA)
                #     cv.putText(face_image, str(i), (u+5, v+5), cv.FONT_HERSHEY_PLAIN, fontScale=0.9,
                #                color=(0, 255, 0), lineType=cv.LINE_AA)
                # cv.imshow('tex', cv.hconcat([cv.cvtColor(isomap, cv.COLOR_BGRA2BGR), face_image]))
                # cv.waitKey(1)

            # # Visualize landmarks thresholded by max value of heatmap
            # for entry, (_landmarks, _maxs) in zip(person_data, landmarks):
            #     face_image = cv.cvtColor(entry['full_face'], cv.COLOR_RGB2BGR)
            #     vis_size = 800
            #     scale = float(vis_size) / face_image.shape[0]
            #     face_image = cv.resize(face_image, (vis_size, vis_size))
            #     for i, ((u, v), m) in enumerate(zip(_landmarks, _maxs)):
            #         u = int(scale * u)
            #         v = int(scale * v)
            #         cv.drawMarker(face_image, (u, v),
            #                       (0, 255, 0) if m > 0.7 else (0, 0, 255),
            #                       cv.MARKER_DIAMOND, markerSize=5, line_type=cv.LINE_AA)
            #         cv.putText(face_image, str(i), (u+5, v+5), cv.FONT_HERSHEY_PLAIN, fontScale=0.9,
            #                    color=(0, 255, 0) if m > 0.7 else (0, 0, 255),
            #                    lineType=cv.LINE_AA)
            #     cv.imshow('face', face_image)
            #     cv.waitKey(1000)

            # Store head poses
            output_pkl = '%s/%s.pkl' % (output_dir, person_id)
            with open(output_pkl, 'wb') as f:
                joblib.dump(output_data, f)
                logger.info('Saved head poses for %s' % person_id)

    def maybe_do_step_4_final_outputs(self):
        self.step_4_final_outputs(self.step_4_input_dir, self.step_4_output_dir)

    def step_4_final_outputs(self, input_dir, output_dir):
        assert_and_ensure_dirs_exist(input_dir, output_dir)

        """
        for person_id in self.person_dirs:
            head_pose_pkl = '%s/%s.pkl' % (input_dir, person_id)
            with open(head_pose_pkl, 'rb') as f:
                head_pose = joblib.load(f)

            # Visualize distribution
            head_pose = np.array([
                list(t3d.euler.mat2euler(cv.Rodrigues(rvec)[0], 'rzyx')) + list(tvec)
                for rvec, tvec in head_pose
                if rvec is not None and tvec is not None
            ])
            plt.ion()
            if len(plt.get_fignums()) == 0:
                plt.figure(figsize=(15, 5))
            else:
                plt.clf()
            plt.subplot(111)
            plt.title(person_id)
            plt.subplot(131)
            plt.hist2d(head_pose[:, 0], head_pose[:, 1], bins=30)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.subplot(132)
            plt.hist2d(head_pose[:, 1], head_pose[:, 2], bins=30)
            plt.xlabel('y')
            plt.ylabel('z')
            plt.subplot(133)
            plt.hist(head_pose[:, 5], bins=30)
            plt.xlabel('d')
            plt.show(block=False)
            plt.pause(0.001)
        """

        for person_id in self.person_dirs:
            raw_data_pkl = '%s/%s.pkl' % (self.step_1_output_dir, person_id)
            with open(raw_data_pkl, 'rb') as f:
                person_data = joblib.load(f)

            landmarks_pkl = '%s/%s.pkl' % (self.step_2_output_dir, person_id)
            with open(landmarks_pkl, 'rb') as f:
                landmarks = joblib.load(f)

            head_pose_pkl = '%s/%s.pkl' % (input_dir, person_id)
            with open(head_pose_pkl, 'rb') as f:
                head_poses = joblib.load(f)

            # Create extra data for MPI case
            normalization_methods = [('zhang', norm.zhang)]
            patch_creation_modes = ['eye-strip']
            if self.dataset_name == 'MPIIFaceGaze':
                patch_creation_modes += ['single-eye-left', 'single-eye-right', 'face']

            for suffix, norm_func in normalization_methods:
                for mode in patch_creation_modes:
                    output_images = []
                    output_heads = []
                    output_3d_gazes = []
                    output_gaze_origins = []
                    output_gaze_targets = []
                    output_inverse_norm_matrices = []
                    for entry, (_landmarks, _), head_pose in zip(person_data, landmarks, head_poses):
                        try:
                            [image, head, gaze_direction, inverse_M, gaze_origin, gaze_target] = \
                                norm_func(entry, _landmarks, head_pose, mode)
                            image = equalize(image)
                            output_images.append(image)
                            output_heads.append(head)
                            output_3d_gazes.append(gaze_direction)
                            output_gaze_origins.append(gaze_origin)
                            output_gaze_targets.append(gaze_target)
                            output_inverse_norm_matrices.append(inverse_M)

                            # # TODO: remove below
                            # cv.imshow('image', cv.equalizeHist(cv.cvtColor(image, cv.COLOR_RGB2GRAY)))
                            # # TODO: draw gaze arrow
                            # cv.waitKey(1)
                        except:
                            if head_pose[0] is not None and head_pose[1] is not None:
                                traceback.print_exc()
                                raise Exception('SOMETHING IS WRONG')
                            continue
                    output_images = np.asarray(output_images).astype(np.uint8)
                    output_heads = np.asarray(output_heads).astype(np.float64)
                    output_3d_gazes = np.asarray(output_3d_gazes).astype(np.float64)
                    output_gaze_origins = np.asarray(output_gaze_origins).astype(np.float64)
                    output_gaze_targets = np.asarray(output_gaze_targets).astype(np.float64)
                    output_inverse_norm_matrices = np.asarray(output_inverse_norm_matrices).astype(np.float64)

                    out_hdf = '%s/%s_%s.h5' % (output_dir, mode, suffix)
                    with h5py.File(out_hdf, 'a' if os.path.isfile(out_hdf) else 'w') as h5f:
                        if person_id in h5f:
                            del h5f[person_id]
                        g = h5f.create_group(person_id)
                        g.create_dataset('image', data=output_images)
                        g.create_dataset('head', data=output_heads)
                        g.create_dataset('gaze', data=output_3d_gazes)
                        g.create_dataset('gaze_origin', data=output_gaze_origins)
                        g.create_dataset('gaze_target', data=output_gaze_targets)
                        g.create_dataset('inverse_M', data=output_inverse_norm_matrices)
                        logger.info('Written %s to %s' % (person_id, out_hdf))
                        h5f.flush()
                        h5f.close()

                if 'single-eye-left' in patch_creation_modes and 'single-eye-right' in patch_creation_modes:
                    # Store a left+right combined single-eye dataset too
                    in_hdf_1 = '%s/single-eye-left_%s.h5' % (output_dir, suffix)
                    in_hdf_2 = '%s/single-eye-right_%s.h5' % (output_dir, suffix)
                    out_hdf = '%s/single-eye_%s.h5' % (output_dir, suffix)
                    with h5py.File(in_hdf_1, 'r') as if1, h5py.File(in_hdf_2, 'r') as if2, \
                            h5py.File(out_hdf, 'a' if os.path.isfile(out_hdf) else 'w') as of:
                        if len(if2[person_id]['image']) == 0:
                            continue
                        if2_image_flipped = np.array([
                            np.fliplr(image)
                            for image in if2[person_id]['image']
                        ])
                        if2_head_flipped = np.copy(if2[person_id]['head'])
                        if2_gaze_flipped = np.copy(if2[person_id]['gaze'])
                        if2_head_flipped[:, 1] = -if2_head_flipped[:, 1]
                        if2_gaze_flipped[:, 1] = -if2_gaze_flipped[:, 1]
                        if person_id in of:
                            del of[person_id]
                        og = of.create_group(person_id)
                        og.create_dataset(
                            'image', data=np.concatenate([if1[person_id]['image'], if2_image_flipped]))
                        og.create_dataset(
                            'head', data=np.concatenate([if1[person_id]['head'], if2_head_flipped]))
                        og.create_dataset(
                            'gaze', data=np.concatenate([if1[person_id]['gaze'], if2_gaze_flipped]))
                        logger.info('Written %s to %s' % (person_id, out_hdf))
                        of.flush()
                        of.close()


def assert_and_ensure_dirs_exist(input_dir, output_dir):
    assert os.path.isdir(input_dir)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

def visualize_head_box(estimator, entry, _landmarks, rvec, tvec):
    # Re-form full frame
    fw, fh = entry['full_frame_size']
    bu, bv, bw, bh = entry['face_bounding_box']
    full_frame = np.zeros((fh, fw, 3), dtype=np.uint8)
    full_frame[bv:bv+bh, bu:bu+bw, :] = cv.cvtColor(entry['full_face'],
                                                    cv.COLOR_RGB2BGR)

    # Black out eyes
    cv.fillConvexPoly(full_frame, np.append(
                        _landmarks[36:42, :].astype(np.int32),
                        [_landmarks[36, :].astype(np.int32)],
                        axis=0),
                      color=(0, 0, 0), lineType=cv.LINE_AA)
    cv.fillConvexPoly(full_frame, np.append(
                        _landmarks[42:48, :].astype(np.int32),
                        [_landmarks[42, :].astype(np.int32)],
                        axis=0),
                      color=(0, 0, 0), lineType=cv.LINE_AA)

    if rvec is not None and tvec is not None:
        fx, fy, cx, cy = entry['camera_parameters']
        camera_matrix = np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]],
                                 dtype=np.float64)

        # Draw fitted model landmarks
        model_landmarks, _ = \
            cv.projectPoints(estimator.sfm_points_ibug_subset, rvec, tvec, camera_matrix, None)
        for u, v in model_landmarks.reshape(-1, 2):
            cv.drawMarker(full_frame, (int(u), int(v)), (0, 255, 0),
                          cv.MARKER_DIAMOND, markerSize=5, line_type=cv.LINE_AA)

        # Draw "box"
        w = 65
        h = w / 4
        d = w / 3
        for a, b, c, t in [
                # ((-w, -h, d), (w, -h, d), (255, 180, 180), 1),
                # ((-w, -h, d), (-w, h, d), (255, 180, 180), 1),
                # ((w, h, d), (-w, h, d), (255, 180, 180), 1),
                # ((w, h, d), (w, -h, d), (255, 180, 180), 1),

                ((-w, -h, -d), (-w, -h, d), (255, 80, 80), 3),
                ((-w, h, -d), (-w, h, d), (255, 80, 80), 3),
                ((w, -h, -d), (w, -h, d), (255, 80, 80), 3),
                ((w, h, -d), (w, h, d), (255, 80, 80), 3),

                ((-w, -h, -d), (w, -h, -d), (255, 0, 0), 6),
                ((-w, -h, -d), (-w, h, -d), (255, 0, 0), 6),
                ((w, h, -d), (-w, h, -d), (255, 0, 0), 6),
                ((w, h, -d), (w, -h, -d), (255, 0, 0), 6),
                ]:
            p1 = tuple(cv.projectPoints(
                np.array(a, dtype=float).reshape(1, 3), rvec, tvec, camera_matrix,
                None)[0][0].astype(np.int32).flatten())
            p2 = tuple(cv.projectPoints(
                np.array(b, dtype=float).reshape(1, 3), rvec, tvec, camera_matrix,
                None)[0][0].astype(np.int32).flatten())
            cv.line(full_frame, p1, p2, c, thickness=2, lineType=cv.LINE_AA)

    cv.imshow('frame', full_frame)
    cv.waitKey(100)

def equalize(image):  # Proper colour image intensity equalization
    ycrcb = cv.cvtColor(image, cv.COLOR_RGB2YCrCb)
    ycrcb[:, :, 0] = cv.equalizeHist(ycrcb[:, :, 0])
    output = cv.cvtColor(ycrcb, cv.COLOR_YCrCb2RGB)
    return output
