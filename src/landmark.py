"""
Copyright 2019 ETH Zurich, Seonwook Park

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import sys

import cv2 as cv
import numpy as np
import tensorflow as tf

sys.path.append(os.path.dirname(__file__) + '/ext/face_hourglass')
from ext.face_hourglass import networks

class LandmarksDetector(object):

    image_width = 256
    num_landmarks = 86
    weights_path = '/ext/face_hourglass/2D86/model.ckpt-217348'

    def __init__(self, batch_size):
        tf.reset_default_graph()

        # Model definition
        self.batch_size = batch_size
        self.input_tensor = \
                tf.placeholder(tf.float32, [batch_size, self.image_width, self.image_width, 3])
        self.model = networks.DNFaceMultiView('')
        with tf.variable_scope('net'):
            self.output_heatmaps, _ = \
                    self.model._build_network(self.input_tensor, datas=None, is_training=False,
                                              n_channels=self.num_landmarks)
            self.output_landmarks = self.tf_softargmax(self.output_heatmaps)
            # self.output_landmarks = self.tf_extract_landmarks(self.output_heatmaps)
            self.output_maxs = tf.reduce_max(self.output_heatmaps, axis=[1, 2])

        # Initialize
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.session = tf.Session(config=config)
        self.session.run(tf.global_variables_initializer())

        # Load weights
        cwd = os.path.dirname(os.path.abspath(__file__))
        self.saver = tf.train.Saver(tf.contrib.slim.get_variables_to_restore())
        self.saver.restore(self.session, cwd + self.weights_path)

    def detect_batch(self, images):
        """Detect facial landmarks in a given batch of pre-cropped face images.

        This code is adapted from github.com/jiankangdeng/Face_Detection_Alignment
        """
        norm_factor = 1. / 255.
        landmarks, maxs = self.session.run(
            [self.output_landmarks, self.output_maxs],
            {
                self.input_tensor: [
                    norm_factor *
                    cv.resize(image, (self.image_width, self.image_width)).astype(np.float32)
                    for image in images],
            },
        )
        # Correct landmarks scale
        scale_correction = np.asarray([
            float(image.shape[0]) / float(self.image_width)
            for image in images
        ])
        landmarks *= scale_correction.reshape(-1, 1, 1)

        return landmarks, maxs

    _softargmax_coords = None

    def tf_softargmax(self, x):
        """Estimate landmark location from heatmaps."""
        with tf.variable_scope('argsoftmax'):
            _, h, w, _ = x.shape.as_list()
            if self._softargmax_coords is None:
                # Assume normalized coordinate [0, 1] for numeric stability
                ref_xs, ref_ys = np.meshgrid(np.linspace(0, 1.0, num=w, endpoint=True),
                                             np.linspace(0, 1.0, num=h, endpoint=True),
                                             indexing='xy')
                ref_xs = np.reshape(ref_xs, [-1, h*w])
                ref_ys = np.reshape(ref_ys, [-1, h*w])
                self._softargmax_coords = (
                    tf.constant(ref_xs, dtype=tf.float32),
                    tf.constant(ref_ys, dtype=tf.float32),
                )
            ref_xs, ref_ys = self._softargmax_coords

            # Assuming N x 18 x 45 x 75 (NCHW)
            beta = 1e2
            x = tf.transpose(x, (0, 3, 1, 2))
            x = tf.reshape(x, [-1, self.num_landmarks, h*w])
            x = tf.nn.softmax(beta * x, axis=-1)
            lmrk_xs = tf.reduce_sum(ref_xs * x, axis=[2])
            lmrk_ys = tf.reduce_sum(ref_ys * x, axis=[2])

            # Return to actual coordinates ranges
            return tf.stack([
                lmrk_xs * (w - 1.0) + 0.5,
                lmrk_ys * (h - 1.0) + 0.5,
            ], axis=2)  # N x 18 x 2

    def tf_extract_landmarks(self, heatmap):
        hs = tf.argmax(tf.reduce_max(heatmap, 2), 1)
        ws = tf.argmax(tf.reduce_max(heatmap, 1), 1)
        lms = tf.transpose(tf.to_float(tf.stack([ws, hs])), perm=[1, 2, 0])
        return lms
