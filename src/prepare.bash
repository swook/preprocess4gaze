#!/bin/bash

# Get face detection model weights
cd ext/tiny_faces
wget -N https://www.cs.cmu.edu/~peiyunh/tiny/hr_res101.mat
python3 matconvnet_hr101_to_pickle.py \
	--matlab_model_path ./hr_res101.mat \
	--weight_file_path ./hr_res101.pkl
cd -

# Get landmarks localization model weights
cd ext/face_hourglass
DRIVE_FILE_ID="1DKTeRlJjyo_tD1EluDjYLhtKFPJ9vIVd"
DRIVE_URL_HEAD="https://drive.google.com/uc?export=download"
DRIVE_TMP_COOKIE="/tmp/gdrive_cookie"
curl -sc $DRIVE_TMP_COOKIE "${DRIVE_URL_HEAD}&id=${DRIVE_FILE_ID}" > /dev/null
DRIVE_CONFIRM_CODE="$(awk '/_warning_/ {print $NF}' $DRIVE_TMP_COOKIE)"
curl -C - -Lb $DRIVE_TMP_COOKIE "${DRIVE_URL_HEAD}&confirm=${DRIVE_CONFIRM_CODE}&id=${DRIVE_FILE_ID}" -o cascade_hourglass_face_alignment_ckpt.zip
rm $DRIVE_TMP_COOKIE
unzip -oq cascade_hourglass_face_alignment_ckpt.zip
cd -
