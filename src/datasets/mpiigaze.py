"""
Copyright 2019 ETH Zurich, Seonwook Park

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import re

import cv2 as cv
import numpy as np
import scipy.io as sio

from dataset import Dataset
from util import Undistorter

import logging
logger = logging.getLogger(__name__)

class MPIIGaze(Dataset):

    def get_person_dirs(self):
        return [
            p for p in os.listdir(self.step_1_input_dir)
            if re.match(r'^p[0-9]{2}$', p)
            and os.path.isdir(self.step_1_input_dir + '/' + p)
        ]

    def preprocess_person_dir(self, person_dir):
        person_id = os.path.basename(os.path.normpath(person_dir))

        # Load annotations
        with open('%s/%s.txt' % (person_dir, person_id), 'r') as f:
            annotations = [line.strip() for line in f.readlines()]

        # Load camera parameters
        camera_related = sio.loadmat(person_dir + '/Calibration/Camera.mat', squeeze_me=True)
        camera_matrix = camera_related['cameraMatrix']
        fx, _, cx, _, fy, cy, _, _, _ = camera_matrix.flatten()
        camera_parameters = np.asarray([fx, fy, cx, cy])
        undistorter = Undistorter(camera_matrix, camera_related['distCoeffs'])

        # Load images
        person_data = []
        for entry in annotations:
            words = entry.split(' ')
            image_path = person_dir + '/' + words[0]
            assert os.path.isfile(image_path)
            image_original = cv.cvtColor(cv.imread(image_path, cv.IMREAD_COLOR), cv.COLOR_BGR2RGB)
            assert image_original is not None
            image_undistorted = undistorter.apply(image_original)
            person_data.append({
                'full_frame': image_undistorted,
                '3d_gaze_target': np.asarray(words[-4:-1]).astype(np.float64),
                'camera_parameters': camera_parameters,
            })
        logger.info('[%s] Read %d images for %s.' % (self.dataset_name, len(person_data), person_id))
        return person_data
