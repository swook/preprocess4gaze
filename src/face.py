"""
Copyright 2019 ETH Zurich, Seonwook Park

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os

import cv2 as cv
import numpy as np
import tensorflow as tf
from scipy.special import expit

from ext.tiny_faces import tiny_face_model, util

class FaceDetector(object):

    def __init__(self, batch_size=1):
        tf.reset_default_graph()

        # Model definition
        self.batch_size = batch_size
        self.input_tensor = tf.placeholder(tf.float32, [batch_size, None, None, 3])

        cwd = os.path.dirname(os.path.abspath(__file__))
        weights_path = cwd + '/ext/tiny_faces/hr_res101.pkl'
        self.model = tiny_face_model.Model(weights_path)
        self.output_tensor = self.model.tiny_face(self.input_tensor)

        # For NMS
        self.nms_boxes_tensor = tf.placeholder(tf.float32, [None, 4])
        self.nms_scores_tensor = tf.placeholder(tf.float32, [None])
        self.nms_max_output_size_tensor = tf.placeholder(tf.int32)
        self.nms_op = tf.image.non_max_suppression(self.nms_boxes_tensor, self.nms_scores_tensor,
                                                   self.nms_max_output_size_tensor,
                                                   iou_threshold=0.1)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.session = tf.Session(config=config)
        self.session.run(tf.global_variables_initializer())

    def detect_batch(self, images):
        """Detect faces in a given batch of images.

        This code is adapted from github.com/cydonia999/Tiny_Faces_in_Tensorflow
        """
        average_image = self.model.get_data_by_key("average_image")
        clusters = self.model.get_data_by_key("clusters")
        clusters_h = clusters[:, 3] - clusters[:, 1] + 1
        clusters_w = clusters[:, 2] - clusters[:, 0] + 1
        normal_idx = np.where(clusters[:, 4] == 1)

        bboxes = [None] * self.batch_size
        for i, _ in enumerate(bboxes):
            bboxes[i] = np.empty(shape=(0, 5))
        for s in (0.1, 0.3):
            # Run through network
            score_final = self.session.run(self.output_tensor, {
                self.input_tensor: [
                    cv.resize(image - average_image, (0, 0), fx=s, fy=s).astype(np.float32)
                    for image in images
                ],
            })
            score_cls = score_final[:, :, :, :25]
            score_reg = score_final[:, :, :, 25:125]
            prob_cls = expit(score_cls)

            def _calc_bounding_boxes(prob_cls, score_reg, score_cls):
                # threshold for detection
                prob_thresh = 0.5
                fy, fx, fc = np.where(prob_cls > prob_thresh)

                # interpret heatmap into bounding boxes
                cy = fy * 8 - 1
                cx = fx * 8 - 1
                ch = clusters[fc, 3] - clusters[fc, 1] + 1
                cw = clusters[fc, 2] - clusters[fc, 0] + 1

                # extract bounding box refinement
                Nt = clusters.shape[0]
                tx = score_reg[:, :, 0:Nt]
                ty = score_reg[:, :, Nt:2*Nt]
                tw = score_reg[:, :, 2*Nt:3*Nt]
                th = score_reg[:, :, 3*Nt:4*Nt]

                # refine bounding boxes
                dcx = cw * tx[fy, fx, fc]
                dcy = ch * ty[fy, fx, fc]
                rcx = cx + dcx
                rcy = cy + dcy
                rcw = cw * np.exp(tw[fy, fx, fc])
                rch = ch * np.exp(th[fy, fx, fc])

                scores = score_cls[fy, fx, fc]
                tmp_bboxes = np.vstack((rcx - rcw / 2, rcy - rch / 2, rcx + rcw / 2, rcy + rch / 2))
                tmp_bboxes = np.vstack((tmp_bboxes / s, scores))
                tmp_bboxes = tmp_bboxes.transpose()
                return tmp_bboxes

            # Calculate and accumulate bounding boxes
            for i in range(score_cls.shape[0]):
                tmp_bboxes = _calc_bounding_boxes(prob_cls[i, :], score_reg[i, :], score_cls[i, :])
                bboxes[i] = np.vstack((bboxes[i], tmp_bboxes))

        # Perform NMS and form output
        out = [None] * self.batch_size
        for i, candidates in enumerate(bboxes):
            if len(candidates) > 0:
                final_bbox_indices = self.session.run(self.nms_op, feed_dict={
                    self.nms_boxes_tensor: candidates[:, :4],
                    self.nms_scores_tensor: candidates[:, 4],
                    self.nms_max_output_size_tensor: candidates.shape[0],
                })
                final_candidates = [candidates[j, :] for j in final_bbox_indices]
                final_bbox = final_candidates[0]
                final_area = (final_bbox[2] - final_bbox[0]) * (final_bbox[3] - final_bbox[1])
                for other_bbox in final_candidates[1:]:
                    # Choose single largest box
                    other_area = (other_bbox[2] - other_bbox[0]) * (other_bbox[3] - other_bbox[1])
                    if other_area > final_area:
                        final_bbox = other_bbox
                out[i] = final_bbox
            else:
                out[i] = None
        return out
