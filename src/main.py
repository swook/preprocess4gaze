#!/usr/bin/env python3
"""
Copyright 2019 ETH Zurich, Seonwook Park

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import argparse
import coloredlogs
import os
import sys

if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='A preprocessing pipeline for gaze direction estimation.')
    parser.add_argument('-v', type=str, help='logging level', default='info',
                        choices=['debug', 'info', 'warning', 'error', 'critical'])
    args = parser.parse_args()
    sys.argv = [sys.argv[0]]  # Don't propagate arguments to imported files

    # Set global log level
    if args.v:
        coloredlogs.install(level=args.v.upper())

    # Preprocess some datasets
    from datasets import *
    dataset_paths = {
        MPIIGaze: '/disks/data2/spark/datasets/MPIIFaceGaze',
    }
    for dataset_class, dataset_path in dataset_paths.items():
        dataset_class(os.path.expanduser(dataset_path))
